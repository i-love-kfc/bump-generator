﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using Pfim;

namespace Bump_Generator
{
    public partial class BumpGenerator : Form
    {
        public BumpGenerator()
        {
            InitializeComponent();
        }

        public enum eConvertMode
        {
            eFromBump,
            eFromNmap,
        }
        private eConvertMode mode = eConvertMode.eFromNmap;

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (!radioButton1.Checked)
                return;

            radioButton2.Checked = false;

            openFileDialog1.Title = "Choose bump map texture";
            mode = eConvertMode.eFromBump;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (!radioButton2.Checked)
                return;

            radioButton1.Checked = false;

            openFileDialog1.Title = "Choose normal map texture";
            mode = eConvertMode.eFromNmap;
        }

        private void SaveTHM(int width, int height, string filename)
        {
            using (BinaryWriter writer = new BinaryWriter(File.Create(filename)))
            {
                bool soc_repaired = checkBox2.Checked;

                writer.Write(0x0810);
                writer.Write(2);
                writer.Write((short)0x0012);

                writer.Write(0x0813);
                writer.Write(5);
                writer.Write(1);
                writer.Write(soc_repaired); // для THM Editor

                writer.Write(0x0812);
                writer.Write(32);
                writer.Write(soc_repaired ? 2 : 3);
                writer.Write((1 << 8) | (1 << 25));
                writer.Write(0);
                writer.Write(0);
                writer.Write(0);
                writer.Write(14);
                writer.Write(width);
                writer.Write(height);

                writer.Write(0x0814);
                writer.Write(4);
                writer.Write(soc_repaired ? 3 : 2);

                writer.Write(0x0815);
                writer.Write(5);
                writer.Write((byte)0);
                writer.Write(1.0F);

                writer.Write(0x0816);
                writer.Write(8);
                writer.Write(1);
                writer.Write(0.5F);

                writer.Write(0x0817);
                writer.Write(9);
                writer.Write(Convert.ToSingle(textBox1.Text));
                writer.Write(1);
                writer.Write((byte)0);

                writer.Write(0x0818);
                writer.Write(1);
                writer.Write((byte)0);

                writer.Write(0x0819);
                writer.Write(1);
                writer.Write((byte)0);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                switch (mode)
                {
                    case eConvertMode.eFromNmap:
                        {
                            var nmap_img = new DDSImage(openFileDialog1.FileName)._image;
                            var ptr_nmap = Marshal.UnsafeAddrOfPinnedArrayElement(nmap_img.Data, 0);

                            PixelFormat format;
                            switch (nmap_img.Format)
                            {
                                case Pfim.ImageFormat.Rgb24:
                                    format = PixelFormat.Format24bppRgb;
                                    break;

                                case Pfim.ImageFormat.Rgba32:
                                    format = PixelFormat.Format32bppArgb;
                                    break;

                                case Pfim.ImageFormat.R5g5b5:
                                    format = PixelFormat.Format16bppRgb555;
                                    break;

                                case Pfim.ImageFormat.R5g6b5:
                                    format = PixelFormat.Format16bppRgb565;
                                    break;

                                case Pfim.ImageFormat.R5g5b5a1:
                                    format = PixelFormat.Format16bppArgb1555;
                                    break;

                                case Pfim.ImageFormat.Rgb8:
                                    format = PixelFormat.Format8bppIndexed;
                                    break;

                                default:
                                    var msg = $"{nmap_img.Format} is not supported yet. Sorry.";
                                    var caption = "Unrecognized format";
                                    MessageBox.Show(msg, caption, MessageBoxButtons.OK);
                                    return;
                            }
                            var bitmap_nmap = new Bitmap(nmap_img.Width, nmap_img.Height, nmap_img.Stride, format, ptr_nmap);

                            var bump_bitmap = new Bitmap(nmap_img.Width, nmap_img.Height, PixelFormat.Format32bppArgb);
                            var errmap_bitmap = new Bitmap(nmap_img.Width, nmap_img.Height, PixelFormat.Format32bppArgb);

                            bool read_height = checkBox1.Checked;
                            for (int x = 0; x < nmap_img.Width; x++)
                            {
                                for (int y = 0; y < nmap_img.Height; y++)
                                {
                                    var R = read_height ? bitmap_nmap.GetPixel(x, y).A : 0; // A
                                    var G = bitmap_nmap.GetPixel(x, y).B; // B
                                    var B = bitmap_nmap.GetPixel(x, y).G; // G
                                    var A = bitmap_nmap.GetPixel(x, y).R; // R
                                    bump_bitmap.SetPixel(x, y, Color.FromArgb(A, R, G, B));

                                    var R2 = bitmap_nmap.GetPixel(x, y).R;
                                    var G2 = bitmap_nmap.GetPixel(x, y).G;
                                    var B2 = bitmap_nmap.GetPixel(x, y).B;
                                    var A2 = read_height ? bitmap_nmap.GetPixel(x, y).A : 0;
                                    var A_err = (128 + 2 * (R - A2) / 3) & 0xff;
                                    var R_err = (128 + 2 * (A - R2) / 3) & 0xff;
                                    var G_err = (128 + 2 * (G - B2) / 3) & 0xff;
                                    var B_err = (128 + 2 * (B - G2) / 3) & 0xff;

                                    errmap_bitmap.SetPixel(x, y, Color.FromArgb(R_err, G_err, B_err, A_err));
                                }
                            }
                            string bump_path = openFileDialog1.FileName.TrimEnd(("nmap" + (openFileDialog1.FileName.EndsWith(".dds") ? ".dds" : ".tga")).ToCharArray()) + "bump.png";
                            string errmap_path = openFileDialog1.FileName.TrimEnd(("nmap" + (openFileDialog1.FileName.EndsWith(".dds") ? ".dds" : ".tga")).ToCharArray()) + "bump#.png";

                            bump_bitmap.Save(bump_path, System.Drawing.Imaging.ImageFormat.Png);
                            errmap_bitmap.Save(errmap_path, System.Drawing.Imaging.ImageFormat.Png);

                            Process ConverterProcess = new Process(); // конверт png в dds
                            ConverterProcess.StartInfo.FileName = Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf('\\')) + "\\nvdxt.exe";
                            ConverterProcess.StartInfo.CreateNoWindow = true;
                            ConverterProcess.StartInfo.RedirectStandardOutput = false;
                            ConverterProcess.StartInfo.RedirectStandardError = false;

                            ConverterProcess.StartInfo.Arguments = $"-file \"{bump_path}\" -dither -dxt5 -Kaiser -RescaleKaiser -output \"{Path.ChangeExtension(bump_path, ".dds")}\"";
                            ConverterProcess.Start();
                            ConverterProcess.WaitForExit();

                            ConverterProcess.StartInfo.Arguments = $"-file \"{errmap_path}\" -dither -dxt5 -Kaiser -RescaleKaiser -output \"{Path.ChangeExtension(errmap_path, ".dds")}\"";
                            ConverterProcess.Start();
                            ConverterProcess.WaitForExit();

                            File.Delete(bump_path);
                            File.Delete(errmap_path);

                            SaveTHM(nmap_img.Width, nmap_img.Height, Path.ChangeExtension(bump_path, ".thm"));
                            break;
                        }
                    case eConvertMode.eFromBump:
                        {
                            var bump_img = new DDSImage(openFileDialog1.FileName)._image;
                            var ptr_bump = Marshal.UnsafeAddrOfPinnedArrayElement(bump_img.Data, 0);

                            PixelFormat format;
                            switch (bump_img.Format)
                            {
                                case Pfim.ImageFormat.Rgb24:
                                    format = PixelFormat.Format24bppRgb;
                                    break;

                                case Pfim.ImageFormat.Rgba32:
                                    format = PixelFormat.Format32bppArgb;
                                    break;

                                case Pfim.ImageFormat.R5g5b5:
                                    format = PixelFormat.Format16bppRgb555;
                                    break;

                                case Pfim.ImageFormat.R5g6b5:
                                    format = PixelFormat.Format16bppRgb565;
                                    break;

                                case Pfim.ImageFormat.R5g5b5a1:
                                    format = PixelFormat.Format16bppArgb1555;
                                    break;

                                case Pfim.ImageFormat.Rgb8:
                                    format = PixelFormat.Format8bppIndexed;
                                    break;

                                default:
                                    var msg = $"{bump_img.Format} is not supported yet. Sorry.";
                                    var caption = "Unrecognized format";
                                    MessageBox.Show(msg, caption, MessageBoxButtons.OK);
                                    return;
                            }
                            var bitmap_bump = new Bitmap(bump_img.Width, bump_img.Height, bump_img.Stride, format, ptr_bump);

                            var errmap_bitmap = new Bitmap(bump_img.Width, bump_img.Height, PixelFormat.Format32bppArgb);

                            for (int x = 0; x < bump_img.Width; x++)
                            {
                                for (int y = 0; y < bump_img.Height; y++)
                                {
                                    var R2 = bitmap_bump.GetPixel(x, y).A;
                                    var G2 = bitmap_bump.GetPixel(x, y).B;
                                    var B2 = bitmap_bump.GetPixel(x, y).G;
                                    var A2 = bitmap_bump.GetPixel(x, y).R;

                                    var R = bitmap_bump.GetPixel(x, y).R;
                                    var G = bitmap_bump.GetPixel(x, y).G;
                                    var B = bitmap_bump.GetPixel(x, y).B;
                                    var A = bitmap_bump.GetPixel(x, y).A;
                                    var A_err = (128 + 2 * (R - A2) / 3) & 0xff;
                                    var R_err = (128 + 2 * (A - R2) / 3) & 0xff;
                                    var G_err = (128 + 2 * (G - B2) / 3) & 0xff;
                                    var B_err = (128 + 2 * (B - G2) / 3) & 0xff;

                                    errmap_bitmap.SetPixel(x, y, Color.FromArgb(R_err, G_err, B_err, A_err));
                                }
                            }
                            string errmap_path = openFileDialog1.FileName.TrimEnd(("bump" + (openFileDialog1.FileName.EndsWith(".dds") ? ".dds" : ".tga")).ToCharArray()) + "bump#.png";

                            errmap_bitmap.Save(errmap_path, System.Drawing.Imaging.ImageFormat.Png);

                            Process ConverterProcess = new Process(); // конверт png в dds
                            ConverterProcess.StartInfo.FileName = Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf('\\')) + "\\nvdxt.exe";
                            ConverterProcess.StartInfo.CreateNoWindow = true;
                            ConverterProcess.StartInfo.RedirectStandardOutput = false;
                            ConverterProcess.StartInfo.RedirectStandardError = false;

                            ConverterProcess.StartInfo.Arguments = $"-file \"{errmap_path}\" -dither -dxt5 -Kaiser -RescaleKaiser -output \"{Path.ChangeExtension(errmap_path, ".dds")}\"";
                            ConverterProcess.Start();
                            ConverterProcess.WaitForExit();

                            File.Delete(errmap_path);
                            break;
                        }
                }

                MessageBox.Show("Bump generated successfully!");
            }
        }
    }
}
